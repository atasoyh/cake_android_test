package com.waracle.androidtest.interactor;

import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.util.Log;

import com.waracle.androidtest.util.StreamUtils;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by atasoyh on 16/08/2017.
 */

public class AsyncLoader extends AsyncTask<String, Void, JSONArray> {

    private final String TAG = AsyncLoader.class.getName();

    Callback callback;

    public AsyncLoader(Callback callback) {
        this.callback = callback;
    }

    @Override
    protected JSONArray doInBackground(String... strings) {
        return loadData(strings[0]);
    }

    @Override
    protected void onPostExecute(JSONArray jsonArray) {
        if (jsonArray != null)
            callback.onSuccess(jsonArray);
        else callback.onFail();
    }

    @Nullable
    private JSONArray loadData(String url) {
        HttpURLConnection urlConnection = null;
        try {
            URL mUrl = new URL(url);
            urlConnection = (HttpURLConnection) mUrl.openConnection();

            InputStream in = new BufferedInputStream(urlConnection.getInputStream());

            // Can you think of a way to improve the performance of loading data
            // using HTTP headers???

            // Also, Do you trust any utils thrown your way????

            byte[] bytes = StreamUtils.readUnknownFully(in);

            // Read in charset of HTTP content.
            String charset = parseCharset(urlConnection.getRequestProperty("Content-Type"));

            // Convert byte array to appropriate encoded string.
            String jsonText = new String(bytes, charset);

            // Read string as JSON.
            return new JSONArray(jsonText);

        } catch (IOException e) {
            Log.e(TAG, e.getLocalizedMessage());
        } catch (JSONException e) {
            Log.e(TAG, e.getLocalizedMessage());
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return null;
    }

    /**
     * Returns the charset specified in the Content-Type of this header,
     * or the HTTP default (ISO-8859-1) if none can be found.
     */
    private static String parseCharset(String contentType) {
        if (contentType != null) {
            String[] params = contentType.split(",");
            for (int i = 1; i < params.length; i++) {
                String[] pair = params[i].trim().split("=");
                if (pair.length == 2) {
                    if (pair[0].equals("charset")) {
                        return pair[1];
                    }
                }
            }
        }
        return "UTF-8";
    }

    public interface Callback {
        void onSuccess(JSONArray jsonArray);

        void onFail();
    }


}

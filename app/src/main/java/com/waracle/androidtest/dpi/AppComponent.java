package com.waracle.androidtest.dpi;

import com.waracle.androidtest.view.dpi.CakesComponent;
import com.waracle.androidtest.view.dpi.CakesModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by atasoyh on 09/07/2017.
 */

@Singleton
@Component(modules = {AppModule.class})
public interface AppComponent {
    CakesComponent plus(CakesModule cakesModule);

}

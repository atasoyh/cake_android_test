package com.waracle.androidtest.view.dpi;

import com.waracle.androidtest.presenter.CakesContract;
import com.waracle.androidtest.presenter.CakesPresenter;

import javax.inject.Inject;
import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * Created by atasoyh on 09/07/2017.
 */
@Module
public class CakesModule {

    private final CakesContract.View view;
    private final String url;

    @Inject
    public CakesModule(CakesContract.View view, String url) {
        this.view = view;
        this.url = url;
    }

    @Provides
    CakesContract.View provideCakesView() {
        return view;
    }

    @Named("CakesURL")
    @Provides
    String provideCakesUrl() {
        return url;
    }

    @Provides
    CakesContract.Presenter provideSearchArtistPresenter(CakesContract.View view, @Named("CakesURL") String url) {
        return new CakesPresenter(view, url);
    }
}


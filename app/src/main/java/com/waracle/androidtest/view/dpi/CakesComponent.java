package com.waracle.androidtest.view.dpi;

import com.waracle.androidtest.view.PlaceholderFragment;

import dagger.Subcomponent;

@CakesScope
@Subcomponent(modules = {CakesModule.class})
public interface CakesComponent {
    void inject(PlaceholderFragment searchMoreFragment);
}
package com.waracle.androidtest.view;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.waracle.androidtest.DefaultApplication;
import com.waracle.androidtest.R;
import com.waracle.androidtest.model.Cake;
import com.waracle.androidtest.presenter.CakesContract;
import com.waracle.androidtest.view.dpi.CakesComponent;
import com.waracle.androidtest.view.dpi.CakesModule;

import java.util.List;

import javax.inject.Inject;

/**
 * Fragment is responsible for loading in some JSON and
 * then displaying a list of cakes with images.
 * Fix any crashes
 * Improve any performance issues
 * Use good coding practices to make code more secure
 */
public class PlaceholderFragment extends ListFragment implements CakesContract.View {

    private static final String TAG = PlaceholderFragment.class.getSimpleName();
    private MyAdapter mAdapter;
    private CakesComponent cakesComponent;
    public static String JSON_URL = "https://gist.githubusercontent.com/hart88/198f29ec5114a3ec3460/" +
            "raw/8dd19a88f9b8d24c23d9960f3300d0c917a4f07c/cake.json";

    @Inject
    public CakesContract.Presenter mPresenter;


    public static PlaceholderFragment newInstance() {

        Bundle args = new Bundle();
        PlaceholderFragment fragment = new PlaceholderFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectDependencies(DefaultApplication.get(getContext()));
    }

    private void injectDependencies(DefaultApplication defaultApplication) {
        cakesComponent = DefaultApplication.get(getContext()).getAppComponent().plus(new CakesModule(this, JSON_URL));
        cakesComponent.inject(this);
    }

    @Override
    public void onDestroy() {
        releaseSubComponents(DefaultApplication.get(getContext()));

        super.onDestroy();

    }

    private void releaseSubComponents(DefaultApplication defaultApplication) {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Create and set the list adapter.
        mAdapter = new MyAdapter(getListView(),getContext());
        setListAdapter(mAdapter);
        mPresenter.loadCakes();

    }

    @Override
    public void showLoading() {

    }

    @Override
    public void dismissLoading() {

    }

    @Override
    public void showUserContents(List<Cake> cakeList) {
        mAdapter.setItems(cakeList);

    }

    @Override
    public void setPresenter(CakesContract.Presenter presenter) {
        this.mPresenter = presenter;
    }
}
package com.waracle.androidtest.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.waracle.androidtest.DefaultApplication;
import com.waracle.androidtest.util.ImageManager;
import com.waracle.androidtest.R;
import com.waracle.androidtest.model.Cake;

import java.util.ArrayList;
import java.util.List;

public class MyAdapter extends BaseAdapter {

    // Can you think of a better way to represent these items???
    private List<Cake> mItems;
    private ListView mListView;
    private final LayoutInflater mInflater;
    public ImageManager imageManager;

    public MyAdapter(ListView listView, Context context) {
        this.mListView = listView;
        this.mItems = new ArrayList<Cake>();
        this.mInflater = LayoutInflater.from(context);
        imageManager =
                new ImageManager(DefaultApplication.get(context), 600000);
    }


    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        if (mItems == null) return 0;
        return mItems.get(position);

    }

    @Override
    public long getItemId(int position) {
        return getItem(position).hashCode();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder viewHolder;
        if (convertView == null) {

            convertView = mInflater.inflate(R.layout.list_item_layout, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        Cake cake = (Cake) getItem(position);
        viewHolder.tvDesc.setText(cake.getDesc());
        viewHolder.tvTitle.setText(cake.getTitle());
        viewHolder.position = position - mListView.getFirstVisiblePosition();
        imageManager.displayImage(cake.getImage(), viewHolder.ivCake, R.mipmap.ic_launcher);
//        new ImageLoader(cake.getImage(), new ImageLoader.Callback() {
//            @Override
//            public void onImageLoaded(Bitmap bitmap) {
//                if (viewHolder.position == position-mListView.getFirstVisiblePosition()) {
//                    viewHolder.ivCake.setImageBitmap(bitmap);
//                }
//            }
//        }).execute();
        return convertView;
    }

    public void setItems(List<Cake> items) {
        mItems = items;
        notifyDataSetChanged();
    }

    static class ViewHolder {
        TextView tvTitle;
        TextView tvDesc;
        ImageView ivCake;
        int position;

        public ViewHolder(View view) {
            this.tvTitle = (TextView) view.findViewById(R.id.title);
            this.tvDesc = (TextView) view.findViewById(R.id.desc);
            this.ivCake = (ImageView) view.findViewById(R.id.image);
            ;
        }
    }
}
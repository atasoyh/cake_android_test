package com.waracle.androidtest;

import android.app.Application;
import android.content.Context;

import com.waracle.androidtest.dpi.AppComponent;
import com.waracle.androidtest.dpi.AppModule;
import com.waracle.androidtest.dpi.DaggerAppComponent;

/**
 * Created by atasoyh on 16/08/2017.
 */

public class DefaultApplication extends Application {

    AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent.builder().appModule(new AppModule(this)).build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

    public static DefaultApplication get(Context context) {
        return (DefaultApplication) context.getApplicationContext();
    }
}

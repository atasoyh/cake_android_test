package com.waracle.androidtest.presenter;

import com.waracle.androidtest.interactor.AsyncLoader;
import com.waracle.androidtest.model.Cake;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by atasoyh on 16/08/2017.
 */

public class CakesPresenter implements CakesContract.Presenter {


    CakesContract.View view;
    String url;

    @Inject
    public CakesPresenter(CakesContract.View view, String url) {
        this.view = view;
        this.url=url;
    }

    @Override
    public void setupListener(CakesContract.Presenter presenter) {
        view.setPresenter(this);
    }

    @Override
    public void loadCakes() {
        view.showLoading();
        new AsyncLoader(new AsyncLoader.Callback() {
            @Override
            public void onSuccess(JSONArray jsonArray) {

                ArrayList<Cake> cakes = new ArrayList<>();
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject object = null;
                    try {
                        object = jsonArray.getJSONObject(i);
                        Cake cake = new Cake();
                        cake.setTitle(object.getString("title"));
                        cake.setDesc(object.getString("desc"));
                        cake.setImage(object.getString("image"));
                        cakes.add(cake);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                view.showUserContents(cakes);
                view.dismissLoading();
            }

            @Override
            public void onFail() {
                view.dismissLoading();

            }
        }).execute(url);
    }
}

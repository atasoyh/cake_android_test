package com.waracle.androidtest.presenter;

import com.waracle.androidtest.model.Cake;

import java.util.List;

/**
 * Created by atasoyh on 16/08/2017.
 */

public class CakesContract {
    public interface View {
        void showLoading();

        void dismissLoading();

        void showUserContents(List<Cake> userContracts);

        void setPresenter(Presenter presenter);
    }

    public interface Presenter {
        void setupListener(Presenter presenter);
        void loadCakes();
    }
}
